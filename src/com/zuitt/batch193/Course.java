package com.zuitt.batch193;

public class Course {
    //Properties
    private String name;

    private String description;

    private int seats;

    private double fee;

    private String startDate;

    private String endDate;

    private User instructor;

    //empty constructor;
    public Course(){
        this.instructor = new User("Jane", "Doe", 25, "Quezon City");
    }

    //parameterized constructor
    public  Course(String name, String description, int seats, double fee, String startDate, String endDate){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    //getters
    public String getName(){return this.name;}
    public String getDescription(){return this.description;}
    public int getSeats(){return this.seats;}
    public double getFee(){return this.fee;}
    public String getStartDate(){return this.startDate;}
    public String getEndDate(){return this.endDate;}
    public String getInstructorFirstName() { return this.instructor.getFirstName(); }

    //setters

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    //methods
    public void courseDetails(){
        System.out.println("Course name:");
        System.out.println(getName());
        System.out.println("Course description:");
        System.out.println(getDescription());
        System.out.println("Course seats:");
        System.out.println(getSeats());
        System.out.println("Course Instructor's first name:");
        System.out.println(getInstructorFirstName());
    }

}
