package com.zuitt.batch193;

public class Main {
    public static void main(String[] args){
        User firstUser = new User("Tee Jae", "Calinao", 25, "Antipolo City");

        Course firstCourse = new Course(){};

        firstCourse.setName("Physics 101");
        firstCourse.setDescription("Learn Physics");
        firstCourse.setSeats(30);

        firstUser.userDetails();
        firstCourse.courseDetails();

    }
}
